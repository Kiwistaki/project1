#define F_CPU 8000000

#include <avr/io.h> 
#include <util/delay_basic.h>
#include <util/delay.h>
#include <avr/interrupt.h>

volatile bool minuterieExpiree = false;
volatile bool bouton = false;
volatile uint16_t cmp = 0;
ISR (INT0_vect)
{
	bouton = true;
	cli();
}

ISR (TIMER1_COMPA_vect)
{
	minuterieExpiree = true;
	cli();
}


void startCompteur(uint16_t duree)
{
  /* Il ne faut pas activer le mode CTC, sinon l'interruption est déclanchée
   * beaucoup trop tôt, même si on met OCR1A (comparateur) à 0xFFFF. Il faut
   * laisser dans le mode par défaut et activer l'interruption sur la comparaison
   * avec OCR1A. La première
   * interruption sera bonne, mais les suivantes se produiront après 0xFFFF cycles
   * (vraisemblablement).
   */
  TCNT1 = 0; //Compteur
  OCR1A = duree; //Valeur à comparer avec le compteur
  //TCCR1A = _BV(WGM11);
  TCCR1B = _BV(CS02) | _BV(CS00) | _BV(WGM12); //  clk / 1024
  TIMSK1 = _BV(OCIE1A); //Interrupt quand OCR1A = TCNT1 
}


int main()
{
  cli();
  DDRD = 0x00;
  DDRA = 0xFF;
  
  EIMSK |= _BV(INT0); //On accepte les interruptions du bouton
  EICRA |= _BV(ISC00);
  sei();
  
  _delay_ms(3000); 
  
  PORTA = 0x55;
  
  _delay_ms(100);
  
  PORTA = 0x00;
  while(PIND & 0x04){}
  bouton = false;
  startCompteur(7812); //Environ 1 sec
  
  while(minuterieExpiree == false && bouton == false) {}
  
  if(minuterieExpiree) {
	  PORTA = 0x55;
  } else {
	  PORTA = 0xAA;
  }
  
  return 0;
}
