/* 
TP3.cpp
Auteurs : Éric Morissette et Félix Prévost
Description : Prob #2 Machine à états finis avec un bouton comme entrée et une LED comme sortie

                    TABLEAU DES ÉTATS
                    
      -----------------------------------------------
      | État présent |  A | État suivant | Sortie Z |
      -----------------------------------------------
      |    INIT_R    |  0 |     INIT_R   |    R     |
      |    INIT_R    |  1 |      DA      |    A     |
      |      DA      |  0 |      DA      |    A     |
      |      DA      |  1 |      UV      |    V     |
      |      UV      |  0 |      UV      |    V     |
      |      UV      |  1 |      DR      |    R     |
      |      DR      |  0 |      DR      |    R     |
      |      DR      |  1 |      UE      |    E     |
      |      UE      |  0 |      UE      |    E     |
      |      UE      |  1 |      DV      |    V     |
      |      DV      |  0 |      DV      |    V     |
      |      DV      |  1 |     INIT_R   |    R     |
      -----------------------------------------------
      
      U = up
      D = down
      R = rouge
      V = vert
      A = ambré
      E = eteint

*/

#define F_CPU 8000000

#include <avr/io.h> 
#include <util/delay_basic.h>
#include <util/delay.h>
#include <avr/interrupt.h>

enum STATE {INIT_R, DA, UV, DR, UE, DV};

volatile STATE state = INIT_R;

//Passer à l'état suivant selon l'entrée A (0 correspond à un 1 dans le tableau en haut de ce fichier,
//	afin de différencier l'appuie et le relâchement du bouton.
void switchState()
{ 
  switch(state)
  {
    case INIT_R:
	state = DA;
      break;
    case DA:
	state = UV;
      break;
    case UV:
	state = DR;
      break;
    case DR:
	state = UE;
      break;
    case UE:
	state = DV;    
      break;
    case DV:
	state = INIT_R;
      break;
      
    default:
      break;
  }
}

//Active la LED selon l'état actuel
void activateLED()
{
  switch(state)
  {
    case INIT_R:
      PORTA = 0xAA;
      break;
    case DA:
      PORTA = 0x55;
      _delay_ms(1);
      PORTA = 0xAA;
      _delay_ms(1);
      break;
    case UV:
      PORTA = 0x55;
      break;
    case DR:
      PORTA = 0xAA;
      break;
    case UE:
      PORTA = 0x00; 
      break;
    case DV:
      PORTA = 0x55;
      break;
      
    default:
      break;
  }
}

ISR (INT0_vect) 
{ 
	 if(PIND & 0x04) {
		 _delay_ms(30);
		 if(PIND & 0x04) {
			 switchState();
		 }
	 } else {
		 switchState();
	 }
	 
	 EIFR |= (1 << INTF0);
}


int main()
{
  cli();
  DDRD = 0x00;
  DDRA = 0xFF;
  
  EIMSK |= _BV(INT0);
  EICRA |= 0b01;
  
  sei();
  
  for(;;) {
	  activateLED();
  }
}
