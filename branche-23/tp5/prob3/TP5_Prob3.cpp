#define F_CPU 8000000

#include <avr/io.h> 
#include <util/delay_basic.h>
#include <util/delay.h>
#include <avr/interrupt.h>

volatile bool minuterieExpiree = false;
volatile bool bouton = false;
volatile uint16_t cmp = 0;


void ajustementPWM(uint8_t dutyCycle)
{
  OCR1A = dutyCycle; //Valeur à comparer avec le compteur
  OCR1B = dutyCycle;
  TCCR1A = _BV(COM1A1) | _BV(COM1B1) | _BV(WGM10);
  TCCR1B =  _BV(CS11); //  clk / 8
}


int main()
{
  DDRD = 0xFF;
  PORTD = 0b11000000;
  for(;;) {
	  ajustementPWM(0);
	 _delay_ms(2000);
	 ajustementPWM(64);
	 _delay_ms(2000);
	 ajustementPWM(128);
	 _delay_ms(2000);
	 ajustementPWM(192);
	 _delay_ms(2000);
	 ajustementPWM(255);
	 _delay_ms(2000);
  }
  
  return 0;
}
