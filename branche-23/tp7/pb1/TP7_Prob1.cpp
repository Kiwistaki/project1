#define F_CPU 8000000

#include <avr/io.h> 
#include <util/delay.h>
#include <avr/interrupt.h>

volatile bool clignote = false;
volatile uint8_t compteur = 0;

ISR (INT0_vect) //Interruption du bouton
{ 
	while((PIND & 0x04) == 0) { //0x04 c'est la pin du bouton
		_delay_ms(100);
		if((PIND & 0x04) == 0) {
			++compteur;
			if(compteur == 120) {
				clignote = true;
				break;
			}
		} else {
			clignote = true;
			break;
		}
	}
	 
	 EIFR |= (1 << INTF0);
}

void initButtonInterrupt()
{
  cli();
  EIMSK |= _BV(INT0);  //Activer interrupt INT0
  EICRA |= _BV(ISC00); //Interrupt on any edge
  sei();
}

int main()
{
  DDRD = 0x00;
  DDRA = 0xFF;
  
  initButtonInterrupt();
  
  for(;;) {
	 if(clignote) {
		 cli();
		 for(uint8_t i = 0; i < 5; ++i) {
			 PORTA = 0b01; //Pin A1
			 _delay_ms(50);
			 PORTA = 0x00; 
			 _delay_ms(50);
		 } 
		 
		 _delay_ms(2000);
		 
		 for(uint8_t i = 0; i < (compteur >> 1) ; ++i) {
			 PORTA = 0b10; //Pin A1
			 _delay_ms(250);
			 PORTA = 0x00;
			 _delay_ms(250);
		 }
		 
		 clignote = false;
		 compteur = 0;
		 
		 sei();
	 }
  }
 
}
