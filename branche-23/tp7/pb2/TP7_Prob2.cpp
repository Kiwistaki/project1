#define F_CPU 8000000

#include <avr/io.h> 
#include <util/delay.h>

#include "can.h"

const uint16_t baudRate = 2400;

void initialisationUART()
{
	UBRR0H = 0;
	UBRR0L = 0xCF;
	
	UCSR0A = 0;
	UCSR0B = _BV(RXEN0) | _BV(TXEN0);
	UCSR0C = _BV(UCSZ00) | _BV(UCSZ01);
}

void transmissionUART(uint8_t donnee)
{
	while((UCSR0A & _BV(UDRE0)) == 0) {}
	UDR0 = donnee;
}

uint8_t receptionUART()
{
	while((UCSR0A & _BV(RXC0)) == 0){}
	return UDR0;
}

int main()
{
  DDRB = 0xFF;
  DDRA = 0x00;
  
  initialisationUART();
  
  can convertisseur;
 
  for(;;) {
	  uint16_t val = convertisseur.lecture(0); //On n'utilise que les 8 premiers bits
	 
	  if(val < 818) {
		  PORTB = 0b01; //Pin B1
	  } else if(val > 940) {
		  PORTB = 0b10; //Pin B2
	  } else {
		 
		PORTB = 0b10; //Pin B2
		_delay_ms(5);
		PORTB = 0b01; //Pin B1
		_delay_ms(15);
	  }
  }
}
