#define F_CPU 8000000

#include <avr/io.h> 
#include <util/delay_basic.h>
#include <alloca.h>

struct PWM 
{
  uint32_t freq;
  uint8_t dutyCycle;
  uint16_t delayDuty;
  uint16_t delayDown;
  uint8_t pin;
};

PWM* createPWM(uint32_t freq, uint8_t dutyCycle, uint8_t pin)
{
  PWM* pwm = (PWM*)alloca(sizeof(PWM));
  pwm->freq = freq;
  pwm->dutyCycle = dutyCycle;
  
  if(pwm->dutyCycle > 100)
    pwm->dutyCycle = 100;
  
  pwm->delayDuty = (20000 / freq) * pwm->dutyCycle;
  pwm->delayDown = (2000000 / freq) - pwm->delayDuty;
  
  
  
  pwm->pin = pin;
  
  return pwm;
}

void generatePWM(PWM* pwm)
{
  PORTA ^= pwm->pin;
  if(pwm->delayDuty != 0)
    _delay_loop_2(pwm->delayDuty);
  PORTA ^= pwm->pin;
  if(pwm->delayDown != 0)
    _delay_loop_2(pwm->delayDown);
}

PWM* changeDutyCycle(PWM* pwm, uint8_t dutyCycle)
{
  pwm->dutyCycle = dutyCycle;
  
  if(pwm->dutyCycle > 100)
    pwm->dutyCycle = 100;
  
  pwm->delayDuty = (20000 / pwm->freq) * pwm->dutyCycle;
  pwm->delayDown = (2000000 / pwm->freq) - pwm->delayDuty;
  
  return pwm;
}

PWM* changeFrequency(PWM* pwm, uint32_t freq)
{
  pwm->freq = freq;
  
  if(pwm->dutyCycle > 100)
    pwm->dutyCycle = 100;
  
  pwm->delayDuty = (20000 / freq) * pwm->dutyCycle;
  pwm->delayDown = (2000000 / freq) - pwm->delayDuty;
  
  return pwm;
}

int main()
{
  DDRA = 0xFF;
  
  //PWM* pwm = createPWM(60, 35, 0x01);
  PWM* pwm2 = createPWM(60, 0, 0x04);
  PORTA = 0x08;
  
  for(;;) {  
   for(int i = 0; i < 120; ++i)
	generatePWM(pwm2);
   changeDutyCycle(pwm2, 25);
   for(int i = 0; i < 120; ++i)
	generatePWM(pwm2);
   changeDutyCycle(pwm2, 50);
   for(int i = 0; i < 120; ++i)
	generatePWM(pwm2);
   changeDutyCycle(pwm2, 75);
   for(int i = 0; i < 120; ++i)
	generatePWM(pwm2);
   changeDutyCycle(pwm2, 100);
   for(int i = 0; i < 120; ++i)
	generatePWM(pwm2);
  
   changeFrequency(pwm2, 3000);
   changeDutyCycle(pwm2, 0);
   
   for(int i = 0; i < 6000; ++i)
	generatePWM(pwm2);
   changeDutyCycle(pwm2, 25);
   for(int i = 0; i < 6000; ++i)
	generatePWM(pwm2);
   changeDutyCycle(pwm2, 50);
   for(int i = 0; i < 6000; ++i)
	generatePWM(pwm2);
   changeDutyCycle(pwm2, 75);
   for(int i = 0; i < 6000; ++i)
	generatePWM(pwm2);
   changeDutyCycle(pwm2, 100);
   for(int i = 0; i < 6000; ++i)
	generatePWM(pwm2);
  }
}
