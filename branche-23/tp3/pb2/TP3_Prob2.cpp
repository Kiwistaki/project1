/* 
TP3.cpp
Auteurs : Éric Morissette et Félix Prévost
Description : Prob #2 Machine à états finis avec un bouton comme entrée et une LED comme sortie

                    TABLEAU DES ÉTATS
                    
      -----------------------------------------------
      | État présent |  A | État suivant | Sortie Z |
      -----------------------------------------------
      |    INIT_R    |  0 |     INIT_R   |    R     |
      |    INIT_R    |  1 |      DA      |    A     |
      |      DA      |  0 |      DA      |    A     |
      |      DA      |  1 |      UV      |    V     |
      |      UV      |  0 |      UV      |    V     |
      |      UV      |  1 |      DR      |    R     |
      |      DR      |  0 |      DR      |    R     |
      |      DR      |  1 |      UE      |    E     |
      |      UE      |  0 |      UE      |    E     |
      |      UE      |  1 |      DV      |    V     |
      |      DV      |  0 |      DV      |    V     |
      |      DV      |  1 |     INIT_R   |    R     |
      -----------------------------------------------
      
      U = up
      D = down
      R = rouge
      V = vert
      A = ambré
      E = eteint

*/

#define F_CPU 8000000

#include <avr/io.h> 
#include <util/delay.h>

enum STATE {INIT_R, DA, UV, DR, UE, DV};

const uint8_t MAX_SAMPLES = 3;
uint8_t samples = 0;
bool buttonPressed = false;

//Retourne l'état du bouton avec un système anti-rebond
bool getButtonState()
{
  if(PIND & 0x04) {
   samples++;
  } else if(samples > 0){
   samples--;
  }
  
  if(samples <= 0) {
    samples = 0;
    buttonPressed = false;
  } else if(samples >= MAX_SAMPLES) {
    samples = MAX_SAMPLES;
    buttonPressed = true;
  }
  
  return buttonPressed;
}

//Passer à l'état suivant selon l'entrée A (0 correspond à un 1 dans le tableau en haut de ce fichier,
//	afin de différencier l'appuie et le relâchement du bouton.
void switchState(STATE* state, bool A)
{ 
  switch(*state)
  {
    case INIT_R:
      if(A)
	*state = DA;
      break;
    case DA:
      if(!A)
	*state = UV;
      break;
    case UV:
      if(A)
	*state = DR;
      break;
    case DR:
      if(!A)
	*state = UE;
      break;
    case UE:
      if(A)
	*state = DV;    
      break;
    case DV:
      if(!A)
	*state = INIT_R;
      break;
      
    default:
      break;
  }
}

//Active la LED selon l'état actuel
void activateLED(STATE state)
{
  switch(state)
  {
    case INIT_R:
      PORTA = 0xAA;
      break;
    case DA:
      PORTA = 0x55;
      _delay_ms(1);
      PORTA = 0xAA;
      _delay_ms(1);
      break;
    case UV:
      PORTA = 0x55;
      break;
    case DR:
      PORTA = 0xAA;
      break;
    case UE:
      PORTA = 0x00; 
      break;
    case DV:
      PORTA = 0x55;
      break;
      
    default:
      break;
  }
}


int main()
{
  DDRD = 0x00;
  DDRA = 0xFF;
  
  STATE state = INIT_R;
  
  for(;;) {
   switchState(&state, PIND & 0x04);
   activateLED(state);
  }
}