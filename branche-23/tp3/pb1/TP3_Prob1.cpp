/* 
TP3.cpp
Auteurs : Éric Morissette et Félix Prévost
Description : Prob #1 Machine à états finis sous forme d'un compteur.

		    TABLEAU DES ÉTATS
                    
      -----------------------------------------------
      | État présent |  A | État suivant | Sortie Z |
      -----------------------------------------------
      |    INIT      |  0 |     INIT     |    0     |
      |    INIT      |  1 |      S1      |    0     |
      |    S1        |  0 |      S1      |    0     |
      |    S1        |  1 |      S2      |    0     |
      |    S2        |  0 |      S2      |    0     |
      |    S2        |  1 |      S3      |    0     |
      |    S3        |  0 |      S3      |    0     |
      |    S3        |  1 |      S4      |    0     |
      |    S4        |  0 |      S4      |    0     |
      |    S4        |  1 |      S5      |    0     |
      |    S5        |  * |     INIT     |    1     |
      -----------------------------------------------

*/

#define F_CPU 8000000

#include <avr/io.h> 
#include <util/delay.h>

enum STATE {INIT, S1, S2, S3, S4, S5};

//Retourne l'état du bouton avec un système atit-rebond
bool getButtonState()
{
  if(PIND & 0x04) {
    _delay_ms(10);
     
    if(PIND & 0x04)
      return true;
  } 
    return false;
}

//Passer à l'état suivant (Assume que l'entrée est à 1)
void switchState(STATE* state)
{
  switch(*state)
  {
    case INIT:
      *state = S1;
      break;
    case S1:
      *state = S2;
      break;
    case S2:
      *state = S3;
      break;
    case S3:
      *state = S4;
      break;
    case S4:
      *state = S5;    
      break;
    case S5:
      *state = INIT;
      break;
      
    default:
      break;
  }
}

int main()
{
  DDRD = 0x00;
  DDRA = 0xFF;
  
  STATE state = INIT;
  
  for(;;) {
    if(getButtonState() == true) {
      while(getButtonState()){}
      switchState(&state);
      
      if(state == S5) {
	PORTA = 0xAA;
	_delay_ms(1000);
	PORTA = 0x00;
	switchState(&state);
      }
    }
  }
}
