#define F_CPU 8000000

#include <avr/io.h> 
#include <util/delay_basic.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <string.h>

#include "memoire_24.h"


int main()
{
  cli();
  DDRD = 0x00;
  DDRA = 0xFF;
  
  Memoire24CXXX mem;
  
  uint8_t mot[40] = "*E*C*O*L*E* *P*O*L*Y*T*E*C*H*N*I*Q*U*E*";
  mot[39] = 0;
  
  mem.ecriture(0, mot, 40);
  
  uint8_t mot2[40];
  
  mem.lecture(0, mot2, 40);
  
  if(strcmp((char*)mot, (char*)mot2) == 0) {
	  PORTA = 0x55;
  } else {
	  PORTA = 0xAA;
  }
}
