#define F_CPU 8000000

#include <avr/io.h> 
#include <util/delay_basic.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <string.h>

const uint16_t baudRate = 2400;

void initialisationUART()
{
	UBRR0H = 0;
	UBRR0L = 0xCF;
	
	UCSR0A = 0;
	UCSR0B = _BV(RXEN0) | _BV(TXEN0);
	UCSR0C = _BV(UCSZ00) | _BV(UCSZ01);
}

void transmissionUART(uint8_t donnee)
{
	while((UCSR0A & _BV(UDRE0)) == 0) {}
	UDR0 = donnee;
}

int main()
{
  cli();
  DDRD = 0xFF;
  DDRA = 0xFF;
  
  initialisationUART();
  
  char mots[21] = "Le robot en INF1995\n";
	uint8_t i, j;
	for ( i = 0; i < 100; i++ ) {
		for ( j=0; j < 20; j++ ) {
			transmissionUART( mots[j] );
		}
	}
}
