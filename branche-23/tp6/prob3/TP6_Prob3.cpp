#define F_CPU 8000000

#include <avr/io.h> 
#include <util/delay_basic.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include <string.h>

#include "memoire_24.h"

const uint16_t baudRate = 2400;

void initialisationUART()
{
	UBRR0H = 0;
	UBRR0L = 0xCF;
	
	UCSR0A = 0;
	UCSR0B = _BV(RXEN0) | _BV(TXEN0);
	UCSR0C = _BV(UCSZ00) | _BV(UCSZ01);
}

void transmissionUART(uint8_t donnee)
{
	while((UCSR0A & _BV(UDRE0)) == 0) {}
	UDR0 = donnee;
}

uint8_t receptionUART()
{
	while((UCSR0A & _BV(RXC0)) == 0){}
	return UDR0;
}

int main()
{
  cli();
  DDRD = 0xFF;
  DDRA = 0xFF;
  
  initialisationUART();
  
  Memoire24CXXX mem;
  
  //uint8_t contenuMem[41] = "*E*C*O*L*E* *P*O*L*Y*T*E*C*H*N*I*Q*U*E*\n";
  //contenuMem[40] = 0xFF;
  
  //mem.ecriture(0x00, contenuMem, 41);
  
  /*uint8_t c = 0;
  uint16_t addr = 0;
  while(true) {
	  mem.lecture(addr, &c, 1);
	  if(c == 0xFF) break;
	  transmissionUART(c); 
	  ++addr;
  }*/
  
  uint8_t str[25] = "Transmission commencee. ";
  
  for(unsigned int i = 0; i < 24; ++i) {
	  transmissionUART(str[i]);
  }
  
  uint8_t var;
  for(;;) {
	  var = receptionUART();
	  transmissionUART(var);
  }
}
