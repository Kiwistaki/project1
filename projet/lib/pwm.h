///////////////////////////////////////////////////////////////////////
//	Fichier:		pwm.h
//	Auteurs:		Yasmine Bekkouche, Eric Morissette, Alexandre Paquet et
//					Felix Prevost
//	Date:			27 fevrier 2013
//	Description:	Cette classe permet la creation d'un signal PWM
//					logiciel.
//////////////////////////////////////////////////////////////////////	

#ifndef _PWM_H_
#define _PWM_H_

#include <avr/interrupt.h>
#include <avr/io.h> 

class PWM
{

public:
	PWM(uint8_t pin5 = 0, uint8_t pin6 = 0)
		: dutyCycle5_(pin5), dutyCycle6_(pin6) 
	{DDRD |= _BV(PD4) | _BV(PD5) | _BV(PD6) | _BV(PD7);}
	~PWM() {};
	void changeDutyCycle(uint8_t dutyCycle); // Methode qui permet de changer le duty cycle du pwm
	void changeDutyCyclePin5(uint8_t dutyCycle); // Methode qui permet de changer le duty cycle du pwm
	void changeDutyCyclePin6(uint8_t dutyCycle); // Methode qui permet de changer le duty cycle du pwm
	
	uint8_t getDutyCyclePin5() const {return dutyCycle5_; }
	uint8_t getDutyCyclePin6() const {return dutyCycle6_; }

private:
  uint8_t dutyCycle5_;
  uint8_t dutyCycle6_;
};

#endif
