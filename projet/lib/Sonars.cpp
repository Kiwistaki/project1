///////////////////////////////////////////////////////////////////////////
//	Fichier:		Sonars.cpp
//	Auteurs:		Yasmine Bekkouche, Éric Morissette, Alexandre Paquet
//					et Félix Prévost
//	Date:			10 avril 2013
//	Description:	Ce programme implemente les sonars.
///////////////////////////////////////////////////////////////////////////

#include "Sonars.h"

bool etape1 = true;

Sonars Sonars::instance;

///////////////////////////////////////////////////////////////////////////
//	Nom:				Constructeur par defaut
//	Parametre(s):		Aucun
//	Valeur de retour:	Aucune
//	Description:		Cree un objet sonar avec les parametres par defaut.
//	Remarque:			Aucune
///////////////////////////////////////////////////////////////////////////
Sonars::Sonars()
	:distance1_(50), distance2_(50), sonar1Actif_(false), reponse(true),
	 ovf1(false), ovf2(false)
{
	cli();
	DDRD |= _BV(PD3);
	DDRD &= ~_BV(PD2);
	DDRB |= _BV(PB3);
	DDRB &= ~_BV(PB2);
	
	EIMSK |= _BV(INT0) | _BV(INT2); //On accepte les interruptions du PD2 et PB2
    EICRA |= _BV(ISC00) | _BV(ISC20); // Pour les Rising et Falling edges
    TCCR0B |= _BV(CS22) | _BV(CS20); // activate the prescaler of 1024
    TCCR2B |= _BV(CS02) | _BV(CS01) | _BV(CS00); // activate the prescaler of 1024
    sei();
}

///////////////////////////////////////////////////////////////////////////
//	Nom:				reset
//	Parametre(s):		Aucun
//	Valeur de retour:	Aucune
//	Description:		Reinitialise les sonars.
//	Remarque:			Aucune
///////////////////////////////////////////////////////////////////////////
void Sonars::reset()
{
	distance1_ = 50;
	distance2_ = 50;
	sonar1Actif_ = false;
	reponse = true;
}

///////////////////////////////////////////////////////////////////////////
//	Nom:				update
//	Parametre(s):		Aucun
//	Valeur de retour:	Aucune
//	Description:		Active un des deux sonars.
//	Remarque:			Aucune
///////////////////////////////////////////////////////////////////////////
void Sonars::update()
{
	if(!sonar1Actif_ && reponse) {
		reponse = false;
		sonar1Actif_ = true;
		if(distance1_ != 0xFF)
			distance1_ *= 2.21; // Transforme la distance en centimetres
		etape1 = true;
		PORTB |= _BV(PB3); // Envoie une impulsion au sonar 1
		_delay_loop_2(20);
		PORTB &= ~_BV(PB3);
	} else if(sonar1Actif_ && reponse) {
		reponse = false;
		sonar1Actif_ = false;
		if(distance2_ != 0xFF)
			distance2_ *= 2.21; // Transforme la distance en centimetres
		etape1 = true;
		PORTD |= _BV(PD3); // Envoie une impulsion au sonar 2
		_delay_loop_2(20);
		PORTD &= ~_BV(PD3);
	}
}

///////////////////////////////////////////////////////////////////////////
//	Nom:				interruption #0
//	Parametre(s):		Aucun
//	Valeur de retour:	Aucune
//	Description:		Va chercher le temps et la distance recues par
//						le sonar 1.
//	Remarque:			Aucune
///////////////////////////////////////////////////////////////////////////
ISR (INT0_vect)
{
	if(etape1) {
		TCNT0 = 0; // Reinitialise le compteur a 0
		Sonars::getInstance().setOverflow1(false);
	} else {
		if(Sonars::getInstance().getOverflow1()) {
			Sonars::getInstance().setDistance1(60);
		} else {
			Sonars::getInstance().setDistance1(TCNT0);
		}
		Sonars::getInstance().repondre();
	}
	etape1 = !etape1;
}

///////////////////////////////////////////////////////////////////////////
//	Nom:				interruption #2
//	Parametre(s):		Aucun
//	Valeur de retour:	Aucune
//	Description:		Va chercher le temps et la distance recues par
//						le sonar 2.
//	Remarque:			Aucune
///////////////////////////////////////////////////////////////////////////
ISR (INT2_vect)
{
	if(etape1) {
		TCNT2 = 0; // Reinitialise le compteur a 0
		Sonars::getInstance().setOverflow2(false);
	} else {
		if(Sonars::getInstance().getOverflow2()) {
			Sonars::getInstance().setDistance2(60);
		} else {
			Sonars::getInstance().setDistance2(TCNT2);
		}
		Sonars::getInstance().repondre();
	}
	etape1 = !etape1;	
}

///////////////////////////////////////////////////////////////////////////
//	Nom:				interruption overflow #0
//	Parametre(s):		Aucun
//	Valeur de retour:	Aucune
//	Description:		Lorsque le compteur #0 overflow.
//	Remarque:			Aucune
///////////////////////////////////////////////////////////////////////////
ISR (TIMER0_OVF_vect)
{
	Sonars::getInstance().setOverflow1(true);
}

///////////////////////////////////////////////////////////////////////////
//	Nom:				interruption #2
//	Parametre(s):		Aucun
//	Valeur de retour:	Aucune
//	Description:		Lorsque le compteur #2 overflow.
//	Remarque:			Aucune
///////////////////////////////////////////////////////////////////////////
ISR (TIMER2_OVF_vect)
{
	Sonars::getInstance().setOverflow2(true);
}
