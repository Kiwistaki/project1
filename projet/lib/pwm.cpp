///////////////////////////////////////////////////////////////////////
//	Fichier:		pwm.cpp
//	Auteurs:		Yasmine Bekkouche, Eric Morissette, Alexandre Paquet et
//					Felix Prevost
//	Date:			27 fevrier 2013
//	Description:	Ce fichier permet l'implementation des methodes de la
//					classe pwm.
//////////////////////////////////////////////////////////////////////

#include "pwm.h"

/////////////////////////////////////////////////////////////////////
//	Nom:				changeDutyCycle
//	Parametres:			dutyCycle
//	Valeur de retour:	aucune
//	Description:		Modifie le dutyCycle du signal pwm sur les broches
//						D5 et D6
//	Remarque:			Aucune
////////////////////////////////////////////////////////////////////
void PWM::changeDutyCycle(uint8_t dutyCycle)
{
  dutyCycle5_ = dutyCycle;
  dutyCycle6_ = dutyCycle;
  
  OCR1A = dutyCycle5_; //Valeur à comparer avec le compteur
  OCR1B = dutyCycle6_;
  TCCR1A = _BV(COM1A1) | _BV(COM1B1) | _BV(WGM10);
  TCCR1B =  _BV(CS11); //  clk / 8
}

/////////////////////////////////////////////////////////////////////
//	Nom:				changeDutyCycle
//	Parametres:			dutyCycle
//	Valeur de retour:	aucune
//	Description:		Modifie le dutyCycle du signal pwm sur la broche D5
//	Remarque:			Aucune
////////////////////////////////////////////////////////////////////
void PWM::changeDutyCyclePin5(uint8_t dutyCycle)
{
   dutyCycle5_ = dutyCycle;
   
   OCR1A = dutyCycle5_; //Valeur à comparer avec le compteur
   TCCR1A = _BV(COM1A1) | _BV(COM1B1) | _BV(WGM10);
   TCCR1B =  _BV(CS11); //  clk / 8
}
/////////////////////////////////////////////////////////////////////
//	Nom:				changeDutyCycle
//	Parametres:			dutyCycle
//	Valeur de retour:	Aucune
//	Description:		Modifie le dutyCycle du signal pwm sur la broche D6
//	Remarque:			Aucune
////////////////////////////////////////////////////////////////////
void PWM::changeDutyCyclePin6(uint8_t dutyCycle)
{
   dutyCycle6_ = dutyCycle;
   
   OCR1B = dutyCycle6_; //Valeur à comparer avec le compteur
   TCCR1A = _BV(COM1A1) | _BV(COM1B1) | _BV(WGM10);
   TCCR1B =  _BV(CS11); //  clk / 8
}
