///////////////////////////////////////////////////////////////////////////
//	Fichier:		final.cpp
//	Auteur:			Yasmine Bekkouche, Éric Morissette, Alexandre Paquet
//					et Félix Prévost
//	Date:			13 avril 2013
//	Description:	Ce programme permet au robot de se déplacer sur le circuit
//					et de detecter les robots sur le circuit
///////////////////////////////////////////////////////////////////////////
//	Remarque:		
///////////////////////////////////////////////////////////////////////////
				
#include "pwm.h"
#include <util/delay.h>
#include "Sonars.h"
#include "SuiveurLigne.h"
#include "SIRCReceveur.h"

#ifndef F_CPU
#define F_CPU 8000000UL
#endif

//On attend un réponse du sonar pour envoyer une distance
uint16_t distanceSonar1(Sonars& s) 
{
	uint16_t dis = s.getDistance1();
	while(dis == 0xFF) {
		s.update();
		dis = s.getDistance1();
	}
	return dis;
}

uint16_t distanceSonar2(Sonars& s) 
{
	uint16_t dis = s.getDistance2();
	while(dis == 0xFF) {
		s.update();
		dis = s.getDistance2();
	}
	return dis;
}

int main()
{	
	PWM roues;
	PORTD = 0xC0;
	DDRA |= _BV(PA6) | _BV(PA7);
	
	initSIRC();
	SuiveurLigne suiveur(125, 32, 2, 512, &roues);
	
	_delay_ms(100);
	Sonars& sonar = Sonars::getInstance();
	sonar.reset();
	
	uint16_t distanceMinS1 = 15;
	uint16_t distanceMinS2 = 25;
	bool rapide = false;
	for(;;)
	{
		messageSIRC mes = getSIRC();
		if(mes == LENT) {
			//PORTA |= _BV(6);
			//PORTA &= ~_BV(7);
			suiveur.setMaxSpeed(125);
			if(rapide) {
				suiveur.changerVoie(false);
				suiveur.setKp(suiveur.getKp() * 0.5);
			}
			rapide = false;
		} else if(mes == RAPIDE) {
			//PORTA |= _BV(7);
			//PORTA &= ~_BV(6);
			suiveur.setMaxSpeed(230);
			if(!rapide) {
				suiveur.changerVoie(true);
				suiveur.setKp(suiveur.getKp() * 2);
			}
			rapide = true;
		}
		suiveur.update();
		
		if(!rapide) continue;
		
		sonar.update();
		
		if(sonar.getDistance1() < distanceMinS1) {
			//Variable utilisée plus bas pour déterminer le temps nécessaire
			//à la stabilisation de la position du sonar qui détermine le retour
			//à la voie rapide
			int16_t control = suiveur.getControl();
			PORTA |= _BV(PA7);
			
			suiveur.changerVoie(true);
			while(suiveur.enChangementDeVoie()) {
				suiveur.update();
				sonar.update();
			}
			
			uint8_t compteur = 120;
			if(control < 50)
			    compteur = 90;
			while(compteur > 0) {
				--compteur;
				suiveur.update();
				sonar.update();
			}
			PORTA &= ~_BV(PA7);
			
			uint16_t d = distanceSonar2(sonar);
			while(d < distanceMinS2) {
				suiveur.update();
				sonar.update();
				d = distanceSonar2(sonar);
			}
			suiveur.changerVoie(false);
		}
	}
}

