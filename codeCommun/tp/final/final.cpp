///////////////////////////////////////////////////////////////////////////
//	Fichier:		tp9.cpp
//	Auteur:			Yasmine Bekkouche, Éric Morissette, Alexandre Paquet
//					et Félix Prévost
//	Date:			11 mars 2013
//	Description:	Ce programme permet l'interprétation d'un programme
//					placé en mémoire et permet de l'exécuter par la suite
///////////////////////////////////////////////////////////////////////////
//	Remarque:		
///////////////////////////////////////////////////////////////////////////
				
#include "uart.h"
#include "pwm.h"
#include "memoire_24.h"
#include <util/delay.h>
#include "lcm_so1602dtr_m_fw.h"
#include "customprocs.h"
#include "Sonars.h"
#include "SuiveurLigne.h"
#include "SIRCReceveur.h"

uint16_t distanceSonar1(Sonars& s) 
{
	uint16_t dis = s.getDistance1();
	while(dis == 0xFF) {
		s.update();
		dis = s.getDistance1();
	}
	return dis;
}

uint16_t distanceSonar2(Sonars& s) 
{
	uint16_t dis = s.getDistance2();
	while(dis == 0xFF) {
		s.update();
		dis = s.getDistance2();
	}
	return dis;
}

int main()
{	
	PWM roues;
	PORTD = 0xC0;
	DDRA |= _BV(PA6) | _BV(PA7);
	
	initSIRC();
	SuiveurLigne suiveur(255, 70, 1.5, 512, &roues);
	
	_delay_ms(100);
	Sonars& sonar = Sonars::getInstance();
	sonar.reset();
	
	uint16_t distanceMinS1 = 15;
	uint16_t distanceMinS2 = 25;
	bool rapide = true;
	for(;;)
	{
		messageSIRC mes = getSIRC();
		if(mes == LENT) {
			PORTA |= _BV(6);
			PORTA &= ~_BV(7);
			suiveur.setMaxSpeed(120);
			if(rapide) {
				suiveur.changerVoie(false);
			}
			rapide = false;
		} else if(mes == RAPIDE) {
			PORTA |= _BV(7);
			PORTA &= ~_BV(6);
			suiveur.setMaxSpeed(255);
			if(!rapide) {
				suiveur.changerVoie(true);
			}
			rapide = true;
		}
		suiveur.update();
		
		if(rapide)
			sonar.update();
		
		if(!rapide) continue;
		
		if(sonar.getDistance1() < distanceMinS1) {
			//Variable utilisée plus bas pour déterminer le temps nécessaire
			//à la stabilisation de la position du conar qui détermine le retour
			//à la voie rapide
			int16_t control = suiveur.getControl();
			
			suiveur.changerVoie(true);
			while(suiveur.enChangementDeVoie()) {
				suiveur.update();
				sonar.update();
			}
			
			uint8_t compteur = 80;
			if(control < 0)
				control = -control;
			if(control < 50)
				compteur = 20;
			while(compteur > 0) {
				--compteur;
				suiveur.update();
				sonar.update();
			}
			
			uint16_t d = distanceSonar2(sonar);
			while(d < distanceMinS2) {
				suiveur.update();
				sonar.update();
				d = distanceSonar2(sonar);
			}
			suiveur.changerVoie(false);
		}
	}
}

