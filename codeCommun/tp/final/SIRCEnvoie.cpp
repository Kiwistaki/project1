///////////////////////////////////////////////////////////////////////////
//	Fichier:		TestSIRCEnvoie.cpp
//	Auteur:			Yasmine Bekkouche, Éric Morissette, Alexandre Paquet
//					et Félix Prévost
//	Date:			27 mars 2013
//	Description:	Ce programme teste la communication SIRC
///////////////////////////////////////////////////////////////////////////
//	Remarque:		
///////////////////////////////////////////////////////////////////////////

#define F_CPU 8000000
#include <avr/delay.h>
#include "../tp8/lib/uart.h"
#include "../tp8/lib/can.h"
#include "../tp8/lib/pwm.h"
#include "../tp8/lib/memoire_24.h"


///////////////////////////////////////////////////////////////////////////
//	Nom:				startComm
//	Parametres:			aucun
//	Valeur de retour:	aucune
//	Description:		Initialise la DEL infrarouge a une frequence de 
//						38kHz.
//	Remarque:			aucune
///////////////////////////////////////////////////////////////////////////
void startComm()
{
  TCCR0A = _BV(WGM01) | _BV(COM0A0) | _BV(COM0B0);
  TCCR0B = _BV(CS01);
  OCR0A = 12;  //valeur calculee pour une frequence de 38 kHz
}

///////////////////////////////////////////////////////////////////////////
//	Nom:				stopComm
//	Parametres:			aucun
//	Valeur de retour:	aucune
//	Description:		Arrete l'emission de la DEL infrarouge
//	Remarque:			aucune
///////////////////////////////////////////////////////////////////////////
void stopComm()
{
	TCCR0A = 0;
	TCCR0B = 0;
}

///////////////////////////////////////////////////////////////////////////
//	Nom:				sendStart
//	Parametres:			aucun
//	Valeur de retour:	aucune
//	Description:		Envoie le signal de depart du protocole SIRC
//	Remarque:			aucune
///////////////////////////////////////////////////////////////////////////
void sendStart()
{
  startComm();
  _delay_us(2400);
  stopComm();
  _delay_us(600);
}

////////////////////////////////////////////////////////////////////////////
//	Nom:				sendCommand
//	Parametres:			comm
//	Valeur de retour:	aucune
//	Description:		Envoie la commande bit a bit en partant du LSB au MSB
//	Remarque:			aucune
////////////////////////////////////////////////////////////////////////////
void sendCommand(uint8_t comm)
{
  for(uint8_t i = 0; i < 7; ++i)  //effectue la boucle 7 fois car la commande est en 7 bits
  {
    startComm();
    if(comm & _BV(i))
	  _delay_us(1200);
	else
      _delay_us(600);
    stopComm();
    _delay_us(600);
  }
}

////////////////////////////////////////////////////////////////////////////
//	Nom:				sendAddress
//	Parametres:			addr
//	Valeur de retour:	aucune
//	Description:		Envoie l'adresse bit a bit en partant du LSB au MSB
//	Remarque:			aucune
////////////////////////////////////////////////////////////////////////////
void sendAddress(uint8_t addr)
{
  for(uint8_t i = 0; i < 5; ++i)  //effectue la boucle 5 fois car l'addresse est en 5 bits
  {
    startComm();
    if(addr & _BV(i))
	  _delay_us(1200);
	else
      _delay_us(600);
    stopComm();
    _delay_us(600);
  }
}

////////////////////////////////////////////////////////////////////////////
//	Nom:				sendInfo
//	Parametres:			comm, addr
//	Valeur de retour:	aucune
//	Description:		Envoie le protocole SIRC au complet
//	Remarque:			aucune
////////////////////////////////////////////////////////////////////////////
void sendInfo(uint8_t comm, uint8_t addr)
{
    sendStart();
    sendCommand(comm);
    sendAddress(addr);
}

int main()
{
  DDRA = 0xff;
  DDRB = 0xFF;
  DDRD = 0x00;
  enum etatSignal {LENT, RAPIDE};
  etatSignal etatduSignal = LENT;

  for (;;)
  {
	while(PIND & 0x04)  //Debouncer pour le boutton poussoir
	{
	  _delay_ms(100);
	  while(PIND & 0x04)
	  {
	    _delay_ms(50);
	  }
	  switch(etatduSignal)
	  {
		case LENT: etatduSignal = RAPIDE;
				   break;
		case RAPIDE: etatduSignal = LENT;
				    break;
	  }
		for(uint8_t i = 0; i < 10; ++i)
		{
		  sendInfo(2, 1);
		  _delay_us(300);
		  sendInfo(3, 1);
		  _delay_us(300);
		  sendInfo(2, 1);
		  _delay_us(300);
		  sendInfo(7, 1);
		  _delay_us(300);
		  if(etatduSignal == RAPIDE){
			sendInfo(19, 1);
			PORTA = 0xAA;
		  }
		  else if(etatduSignal == LENT){
			sendInfo(18, 1);
			PORTA = 0x55;
		  }
		  _delay_us(300);
		}
		PORTA = 0x00;
	}
  }
}
