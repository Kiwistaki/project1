///////////////////////////////////////////////////////////////////////////
//	Fichier:		SuiveurLigne.h
//	Auteurs:		Yasmine Bekkouche, Éric Morissette, Alexandre Paquet
//					et Félix Prévost
//	Date:			11 mars 2013
//	Description:	Cet en-tete implemente les fonctions de suiveur de ligne.
///////////////////////////////////////////////////////////////////////////
//	Remarque:		Connectez les fils jaune, bleu et violet sur les pins
//					2, 4 et 6 respectivement.
///////////////////////////////////////////////////////////////////////////
#ifndef SUIVEURLIGNE_H
#define SUIVEURLIGNE_H

#include "pwm.h"
#include <util/delay.h>
#include <avr/io.h>
#include "Sonars.h"

#define DROITE _BV(PA5)
#define GAUCHE _BV(PA1)
#define CENTRE _BV(PA3)

//B = blanc    N = Noir
enum State {BBB, BBN, BNB, BNN, NBB, NBN, NNB, NNN, NBBB, BBBN};

class SuiveurLigne
{
public:
	SuiveurLigne(uint8_t maxSpeed, float kp, float ki, float kd, PWM* roues);
	
	void changerVoie(bool gauche);
	void update();
	void reset();
	void setMaxSpeed(uint8_t maxSpeed) {maxSpeed_ = maxSpeed;}
	int16_t getControl() const {return control_;}
	bool enChangementDeVoie() const {return changementDeVoie_;}
	
	static uint8_t getCorrectionRoues() {return correctionRoues_;}
private:
	uint8_t maxSpeed_;
	
	const float kp_;
	const float ki_;
	const float kd_;
	
	int16_t integral_;
	int8_t lastErreur_;
	int16_t control_;
	bool changementDeVoie_;
	bool voieGauche_;
	uint8_t compteurRetour_; //compteur utilisé après un changement de voie pour 
							 //déterminer quand ça fait trop longtemps qu'il est
							 //en changement de voie et qu'il doit commencer à
							 //revenir.
	
	PWM* roues_;
	
	State lastState_;
	State state_;

	State lireCapteur();
	int8_t detPos(State state);
	
	
	static uint8_t correctionRoues_;
};

void avancer(uint8_t speed, PWM& pwm);
void tournerDroite(uint8_t speed, uint16_t diff, PWM& pwm);
void tournerGauche(uint8_t speed, uint16_t diff, PWM& pwm);

#endif
