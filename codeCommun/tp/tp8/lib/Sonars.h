///////////////////////////////////////////////////////////////////////
//	Fichier:		Sonars.h
//	Auteurs:		Yasmine Bekkouche, Eric Morissette, Alexandre Paquet et
//					Felix Prevost
//	Date:			10 avril 2013
//	Description:	Cet en-tete implemente les fonctions de sonars.
////////////////////////////////////////////////////////////////////// 
#ifndef SONARS_H
#define SONARS_H

#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

class Sonars
{
public:
	static Sonars& getInstance() {return instance;}

	void update();
	
	uint16_t getDistance1() { 
		uint16_t d = distance1_;
		distance1_ = 0xFF;
		return d; 
	}
	uint16_t getDistance2() { 
		uint16_t d = distance2_;
		distance2_ = 0xFF;
		return d; 
	}
	void setDistance1(uint16_t dist) { distance1_ = dist; }
	void setDistance2(uint16_t dist) { distance2_ = dist; }
	void repondre() { reponse = true; }
	void setOverflow1(bool ovf) {ovf1 = ovf;}
	void setOverflow2(bool ovf) {ovf2 = ovf;}
	bool getOverflow1() const {return ovf1;}
	bool getOverflow2() const {return ovf2;}
	void reset();
private:
	Sonars();

	uint16_t distance1_;
	uint16_t distance2_;
	bool sonar1Actif_;
	bool reponse;
	bool ovf1;
	bool ovf2;
	
	static Sonars instance;
};

#endif
