///////////////////////////////////////////////////////////////////////////
//	Fichier:		SIRCEnvoie.cpp
//	Auteurs:		Yasmine Bekkouche, Éric Morissette, Alexandre Paquet
//					et Félix Prévost
//	Date:			27 mars 2013
//	Description:	Ce programme teste la communication SIRC
///////////////////////////////////////////////////////////////////////////

#include <util/delay.h>
#include <avr/io.h>
#include "SIRCReceveur.h"
#include "uart.h"

///////////////////////////////////////////////////////////////////////////
//	Nom:				lecture
//	Parametre(s):		commande, addresse, posLecture, commandeRobot
//	Valeur de retour:	Aucune
//	Description:		Lit la commande et l'adresse d'un protocole SIRC et 
//						l'ecrit dans le tableau de commande si elle est
//						valable.
//	Remarque:			Aucune
///////////////////////////////////////////////////////////////////////////
void lecture(uint8_t* commande, uint8_t addresse, uint8_t& posLecture, const uint8_t* commandeRobot)
{
  // Au debut de cette fonction, on est a T + 2700
  uint8_t addresseTemp = 0;
  uint8_t commandeTemp = 0;
  uint8_t upTimer = 0;

  _delay_us(600); // On est maintenant au debut de la commande

  for(uint8_t commi = 0; commi < 7; ++commi) // Lire les 7 bits de la commande
  {
    upTimer = 0;
    while(!(PINB & _BV(PB0))) // Tant qu'on recoit un signal haut, on augmente de 6 et on va attendre 600us
    {
      upTimer += 6;
      _delay_us(600);
    }
    if(upTimer == 6) // lire un "0"
      commandeTemp &= ~(_BV(commi));
    else if (upTimer == 12) // Lire un "1"
      commandeTemp |= _BV(commi);
    _delay_us(600); // Se positionner sur la prochaine valeur
  }

  for(uint8_t addri = 0; addri < 5; ++addri) // Lire les 5 bits de l'addresse
  {
    upTimer = 0;
    while(!(PINB & _BV(PB0))) // Tant qu'on recoit un signal haut, on augmente de 6 et on va attendre 600us
    {
      upTimer += 6;
      _delay_us(600);
    }
    if(upTimer == 6) // lire un "0"
      addresseTemp &= ~(_BV(addri));
    else if (upTimer == 12) // Lire un "1"
      addresseTemp |= _BV(addri);
    _delay_us(600); // Se positionner sur la prochaine valeur
  }

  if(addresseTemp == addresse)
  {
	if(posLecture < 4) // Si on est en train de lire un des quatre premieres commandes, soit notre "addresse" de robot
	{
	  if(commandeRobot[posLecture] == commandeTemp) // Si la commande est valable, on l'ecrit dans le tableau de commandes
	  {
		commande[posLecture] = commandeTemp;
		posLecture++;
	  }else // Si la commande n'est pas valable, on reset notre position de lecture
		posLecture = 0;
	}else if (posLecture == 4) // Si on est a la 5ieme commande, on la copie peut importe ce qu'elle est
	{
	  commande[posLecture] = commandeTemp;
	  posLecture = 10;
    }
  }else // Si l'addresse n'est pas bonne, on veut faire en sorte de sortir du "while(posLecture < 5)"
  posLecture = 254;
}

///////////////////////////////////////////////////////////////////////////
//	Nom:				lectureSIRC
//	Parametre(s):		Aucun
//	Valeur de retour:	Un enum messageSIRC
//	Description:		Lit des protocoles SIRC jusqu'a l'obtention d'une 
//						suite de commandes valables et retourne un enum 
//						selon les protocoles lus.
//	Remarque:			Aucune
///////////////////////////////////////////////////////////////////////////
messageSIRC lectureSIRC()
{
  messageSIRC messageLu = ERREUR;
  uint8_t upTimer = 0;
  uint8_t commande[5] = {0,0,0,0,0};
  const uint8_t commandeAvance = 19;
  const uint8_t commandeRalenti = 18;
  const uint8_t commandeRobot[4] = {2,3,2,7};
  const uint8_t addresse = 1;
  uint8_t posLecture = 0;

    if(!(PINB & _BV(PB0)))
    {
      while(posLecture < 5) // Lit 5 valeurs, a moins qu'on ne lise pas une valeur valable
      {
		_delay_us(300);
		while(!(PINB & _BV(PB0)) && upTimer < 24) // Tant qu'on recoit un signal haut, on augmente de 6 et on va attendre 600us
		{
		  upTimer += 6;
		  _delay_us(600);
		}
		if(upTimer == 24) // Si on voit un signal de 2400us, c'est un debut de protocole SIRC et on va appeler lecture
		  lecture(commande, addresse, posLecture, commandeRobot);
		upTimer = 0;
		if(posLecture < 4) // Attendre le prochain protocole
		{
		  while(PINB & _BV(PB0))
		  {
			  upTimer += 1;
			  _delay_us(100);
			  if(upTimer > 12)
				return ERREUR;
		  }
		  upTimer = 0;
		}
	  }
	}

	// Retourne un bool selon la derniere valeur lue dans les protocoles SIRC
	if(commande[4] == commandeAvance)
		messageLu = RAPIDE;
	else if(commande[4] == commandeRalenti)
		messageLu = LENT;
	else
		messageLu = ERREUR;
	posLecture = 0;

  return messageLu;
}

///////////////////////////////////////////////////////////////////////////
//	Nom:				getSIRC
//	Parametre(s):		Aucun
//	Valeur de retour:	Un enum messageSIRC
//	Description:		Verifie s'il y a un signal, si oui va appeler la
//						fonction lectureSIRC()
//	Remarque:			Aucune
///////////////////////////////////////////////////////////////////////////
messageSIRC getSIRC()
{
	if(!(PINB & _BV(PB0))) {
	  return lectureSIRC();
	} else{
	  _delay_us(600);
	  if(!(PINB & _BV(PB0))) {
	    return lectureSIRC();
	  } else {
		return ERREUR;
	  }
    }
}

///////////////////////////////////////////////////////////////////////////
//	Nom:				initSIRC
//	Parametre(s):		Aucun
//	Valeur de retour:	Aucune
//	Description:		Initialise ce qu'il faut pour le SIRC
//	Remarque:			Aucune
///////////////////////////////////////////////////////////////////////////
void initSIRC()
{
	DDRD |= _BV(PD1);
	DDRB &= ~_BV(PB0) & ~_BV(PD0);
	
	PORTD |= _BV(PD1);
	//PORTD &= ~_BV(PD0); 
}
