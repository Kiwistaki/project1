///////////////////////////////////////////////////////////////////////
//	Fichier:		uart.cpp
//	Auteurs:		Yasmine Bekkouche, Eric Morissette, Alexandre Paquet et
//					Felix Prevost
//	Date:			27 fevrier 2013
//	Description:	Ce fichier permet l'implementation des methodes de la
//					classe uart.
//////////////////////////////////////////////////////////////////////

#include "uart.h"

/////////////////////////////////////////////////////////////////////
//	Nom:				Constructeur par defaut
//	Parametres:			Aucun
//	Valeur de retour:	Aucune
//	Description:		Creer un objet de type UART
//	Remarque:			Aucune
////////////////////////////////////////////////////////////////////
UART::UART()
{
	UBRR0H = 0;
	UBRR0L = 0xCF;
	
	UCSR0A = 0;
	UCSR0B = _BV(RXEN0) | _BV(TXEN0);
	UCSR0C = _BV(UCSZ00) | _BV(UCSZ01);
}

/////////////////////////////////////////////////////////////////////
//	Nom:				transmissionUART
//	Parametres:			donnee
//	Valeur de retour:	Aucune
//	Description:		Transmet la donnee vers l'ordinateur
//	Remarque:			Aucune
////////////////////////////////////////////////////////////////////
void UART::transmissionUART(uint8_t donnee) const
{
	while((UCSR0A & _BV(UDRE0)) == 0) {}
	UDR0 = donnee;
}

/////////////////////////////////////////////////////////////////////
//	Nom:				receptionUART
//	Parametres:			Aucun
//	Valeur de retour:	URD0, une valeur 8-bit
//	Description:		Recoit une donnee 8-bit de l'ordinateur
//	Remarque:			Aucune
////////////////////////////////////////////////////////////////////
uint8_t UART::receptionUART() const
{
	while((UCSR0A & _BV(RXC0)) == 0) {}
	return UDR0;
}
