///////////////////////////////////////////////////////////////////////////
//	Fichier:		SuiveurLigne.cpp
//	Auteurs:		Yasmine Bekkouche, Éric Morissette, Alexandre Paquet
//					et Félix Prévost
//	Date:			11 mars 2013
//	Description:	Cet programme suit la ligne.
///////////////////////////////////////////////////////////////////////////
//	Remarque:		Connectez les fils jaune, bleu et violet sur les pins
//					2, 4 et 6 respectivement.
///////////////////////////////////////////////////////////////////////////
#include "SuiveurLigne.h"

uint8_t SuiveurLigne::correctionRoues_ = 8;

SuiveurLigne::SuiveurLigne(uint8_t maxSpeed, float kp, float ki, float kd, PWM* roues)
	: maxSpeed_(maxSpeed), kp_(kp), ki_(ki), kd_(kd), integral_(0), lastErreur_(0),
	  changementDeVoie_(false), voieGauche_(false), compteurRetour_(0), roues_(roues),
	   lastState_(BNB), state_(BNB)
{
	DDRA &= ~_BV(PA1) & ~_BV(PA3) & ~_BV(PA5);	
}

///////////////////////////////////////////////////////////////////////////
//	Nom:				lireCapteur
//	Parametre(s):		Aucun
//	Valeur de retour:	State
//	Description:		Lit les ports correspondants au suiveur de ligne et
//						retourne un état correspondant à l'état du capteur.
//	Remarque:			Aucune
//////////////////////////////////////////////////////////////////////////
State SuiveurLigne::lireCapteur()
{
	uint8_t s = 0;
	if(!(PINA & DROITE)) {
		s |= 0x01;
	}
	if(!(PINA & CENTRE)) {
		s |= 0x02;
	}
	if(!(PINA & GAUCHE)) {
		s |= 0x04;
	}
	return State(s);
}

///////////////////////////////////////////////////////////////////////////
//	Nom:				detPos
//	Parametre(s):		State state
//	Valeur de retour:	uint8_t
//	Description:		Détermine la position du robot par rapport à la
//						ligne noire selon l'état du suiveur de ligne
//						(state) et retourne la position (-2, -1, 0, 1, 2, etc)
//	Remarque:			Aucune
///////////////////////////////////////////////////////////////////////////
int8_t SuiveurLigne::detPos(State state)
{
	state_ = state;
	if(state_ == BBB) {
		if(lastState_ == NBBB) {
			lastState_ = NBBB;
			return -2;
		} else if(lastState_ == BBBN) {
			lastState_ = BBBN;
			return 2;
		} else if(lastState_ == NBB) {
			lastState_ = NBBB;
			return -2;
		} else if(lastState_ == BBN) {
			lastState_ = BBBN;
			return 2;
		} else {
			lastState_ = BNB;
			return 0;
		}
	} else {
		lastState_ = state_;
		switch(state_)
		{
		case BNB:
			return 0;
			break;
		case NBB:
			return -1;
			break;
		case BBN:
			return 1;
			break;
		case NNB:
			if(maxSpeed_ > 230) //Robot rapide
				return -1;
			else 				//Robot lent
				return 2;
			break;
		case BNN:
			if(maxSpeed_ > 230)
				return 0;
			else
				return 2;
			break;
		case NBN:
			if(maxSpeed_ > 230)
				return -1;
			else
				return 2;
			break;
		case NNN:
			return 3;
		default:
			return 0;
			break;
		}
	}
}

///////////////////////////////////////////////////////////////////////////
//	Nom:				changerVoie
//	Parametre(s):		bool gauche
//	Valeur de retour:	Aucune
//	Description:		Change de voie vers la gauche si gauche est vrai
//						Cette fonction se termine quand le robot a quitté
//						sa ligne courante.
//	Remarque:			Aucune
///////////////////////////////////////////////////////////////////////////
void SuiveurLigne::changerVoie(bool gauche)
{
	if(changementDeVoie_)
		return;
		
	if(integral_ > 255)
		integral_ = 255;
	if(integral_ < -255)
		integral_ = -255;
		
	if(gauche) {
		uint16_t diff = 190 + integral_ / 1.5;
		
		if(diff < 0)
			diff = -diff;
			
		tournerGauche(maxSpeed_, diff , *roues_); //On quitte la ligne
		_delay_ms(300);
		
		State s = NNN;
		while(s != BBB) { //Tant qu'on ne retrouve pas la ligne
			s = lireCapteur();
			if(s == BBB) {
				_delay_ms(50);
				s = lireCapteur();
			}
		}
		
		reset(); //On fait avancer le robot droit devant
		changementDeVoie_ = true;
		voieGauche_ = true;
	} else {
		uint16_t diff = 190 - integral_;
		if(diff < 0)
			diff = -diff;
		tournerDroite(maxSpeed_, diff, *roues_);
		_delay_ms(200);
		
		State s = NNN;
		while(s != BBB) {
			s = lireCapteur();
			if(s == BBB) {
				_delay_ms(100);
				s = lireCapteur();
			}
		}
		reset();
		lastErreur_ = -2;
		lastState_ = BBBN;
		changementDeVoie_ = true;
		voieGauche_ = false;
	}
	compteurRetour_ = 25;
}

///////////////////////////////////////////////////////////////////////////
//	Nom:				update
//	Parametre(s):		Aucun
//	Valeur de retour:	Aucune
//	Description:		Met à jour la vitesse des roues selon plusieurs 
//						paramètres internes à l'objet. Algorithme inspirée
//						de l'algorithme PID (Proportionnal Integral Derivative)
//	Remarque:			Aucune
///////////////////////////////////////////////////////////////////////////
void SuiveurLigne::update()
{
	
	int8_t erreur = detPos(lireCapteur());
	
	uint8_t maxSpeed = maxSpeed_;
	
	int16_t proportional = -erreur;
		
	//Si |integrale| > |maxSpeed|
	//|integral| = |maxspeed|
	integral_ += proportional;
	if(integral_ > maxSpeed_)
		integral_ = maxSpeed_;
	else if(integral_ < -maxSpeed_)
		integral_ = -maxSpeed_;
		
	//Quand on revient au centre, on veut reset l'integrale
	//pour qu'on ne continue pas à tourner
	if(erreur == 0)
		integral_ = 0;
	
	//Si on change de voie et que le compteur activé au changement de voie est > 0
	if(compteurRetour_ > 0 && changementDeVoie_) {
		--compteurRetour_;
		//Si le compteur est = à 0, on commence à tourner pour retrouver une 
		//trajectoire droite, car le robot est en changement de ligne depuis
		//trop longtemps et risque d'arrive sur la ligne perpendiculairement.
		if(compteurRetour_ == 0) {
			if(voieGauche_) {
				tournerDroite(maxSpeed_, 200, *roues_);
				_delay_ms(200);
			} else {
				tournerDroite(maxSpeed_, 200, *roues_);
				_delay_ms(200);
			}
		}
	}
	
	//Si, après un changement de voie, on détecte une erreur, c'est que le
	//robot a retrouvé la ligne noire, et on donne un coup dans le sens
	//inverse de la direction
	if(changementDeVoie_ && erreur != 0) {
		if(!voieGauche_) {
			tournerGauche(maxSpeed_, 200, *roues_);
			_delay_ms(400);
		} else {
			tournerDroite(maxSpeed_, 200, *roues_);
			_delay_ms(400);
		}
	}
			
	int16_t derivative = erreur - lastErreur_;
	derivative = -derivative;
		
	//Valeur de control ( 0 à 255 ) utilisée pour le paramètre diff dans
	//tournerDroite() et tournerGauche()
	control_ = proportional * kp_ + integral_ * ki_ + derivative * kd_;
	
	
	//Si |control| > |maxSpeed|
	//|control| = |maxspeed|	
	if(control_ > maxSpeed)
		control_ = maxSpeed;
	else if(control_ < -maxSpeed)
		control_ = -maxSpeed;
	
	if(control_ < 0) {
		tournerDroite(maxSpeed, -control_, *roues_);
	} else {
		tournerGauche(maxSpeed, control_, *roues_);
	}
		
	//Si la dérivative n'est pas nulle, on fait un délai pour que son effect
	//soit ressenti.
	lastErreur_ = erreur;
	if(derivative != 0) {
		int16_t cmpt = integral_ * ki_;
		if(cmpt < 0)
			cmpt = -cmpt;
		if(changementDeVoie_ && cmpt > 500)
			cmpt = 500;
		for(uint8_t i = 0; i < cmpt+30; ++i)
			_delay_ms(1);
		integral_ = 0;
		changementDeVoie_ = false;
	}
	_delay_ms(12);
}

///////////////////////////////////////////////////////////////////////////
//	Nom:				reset
//	Parametre(s):		Aucun
//	Valeur de retour:	Aucune
//	Description:		Ré-initialise l'état du suiveur de ligne à l'état où
//						la ligne est centré sur le robot.
//	Remarque:			Aucune
///////////////////////////////////////////////////////////////////////////
void SuiveurLigne::reset()
{
	lastState_ = BNB;
	state_ = BNB;
	integral_ = 0;
	control_ = 0;
	lastErreur_ = 0;
}

///////////////////////////////////////////////////////////////////////////
//	Nom:				avancer
//	Parametre(s):		uint8_t speed, PWM& pwm
//	Valeur de retour:	Aucune
//	Description:		Faut avancer le robot contrôlé par le PWM pwm en ligne
//						droite à la vitesse speed (255 max)
//	Remarque:			Aucune
///////////////////////////////////////////////////////////////////////////
void avancer(uint8_t speed, PWM& pwm)
{
	pwm.changeDutyCyclePin5(speed-8);
	pwm.changeDutyCyclePin6(speed);
}

///////////////////////////////////////////////////////////////////////////
//	Nom:				tournerDroite
//	Parametre(s):		uint8_t speed, uint16_t diff, PWM& pwm
//	Valeur de retour:	Aucune
//	Description:		Faut avancer le robot contrôlé par le PWM pwm à droite
//						en mettant la roue gauche à la vitesse speed et la
//						roue droite à la vitesse speed - diff.
//	Remarque:			Aucune
///////////////////////////////////////////////////////////////////////////
void tournerDroite(uint8_t speed, uint16_t diff, PWM& pwm)
{
	uint8_t max = speed - SuiveurLigne::getCorrectionRoues();
	if(diff > max)
		diff = max;
	pwm.changeDutyCyclePin5(speed - SuiveurLigne::getCorrectionRoues() - diff);
	pwm.changeDutyCyclePin6(speed);
}

///////////////////////////////////////////////////////////////////////////
//	Nom:				tournerGauche
//	Parametre(s):		uint8_t speed, uint16_t diff, PWM& pwm
//	Valeur de retour:	Aucune
//	Description:		Faut avancer le robot contrôlé par le PWM pwm à gauche
//						en mettant la roue droite à la vitesse speed et la
//						roue gauche à la vitesse speed - diff.
//	Remarque:			Aucune
///////////////////////////////////////////////////////////////////////////
void tournerGauche(uint8_t speed, uint16_t diff, PWM& pwm)
{
	if(diff > speed)
		diff = speed;
	pwm.changeDutyCyclePin5(speed-SuiveurLigne::getCorrectionRoues());
	pwm.changeDutyCyclePin6(speed - diff);
}
