///////////////////////////////////////////////////////////////////////////
//	Fichier:		tp9.cpp
//	Auteur:			Yasmine Bekkouche, Éric Morissette, Alexandre Paquet
//					et Félix Prévost
//	Date:			11 mars 2013
//	Description:	Ce programme permet l'interprétation d'un programme
//					placé en mémoire et permet de l'exécuter par la suite
///////////////////////////////////////////////////////////////////////////
//	Remarque:		Pour que tout fonctionne comme supposé, certains 
//					branchements sont nécessaires. Tout d'abord, branchez
//					le fil de la DEL sur les pins 0 et 1 du le port B. 
//					Ensuite, branchez le fil qui sert pour le moteur 
//					gauche sur les pins 6 et 8 du port D. Branchez le fil
//					pour le moteur droit sur pins 5 et 7 du port D. 
///////////////////////////////////////////////////////////////////////////

#define F_CPU 8000000				
#include "../tp8/lib/uart.h"
#include "../tp8/lib/can.h"
#include "../tp8/lib/pwm.h"
#include "../tp8/lib/memoire_24.h"
#include <avr/delay.h>

uint16_t notesMIDI[37] = {110, 117, 123, 131, 139, 147, 156, 165, 175, 185, 196, 208, 220, 
						  233, 247, 262, 277, 294, 311, 330, 350, 370, 392, 415, 440, 
						  466, 494, 523, 554, 587, 622, 659, 698, 740, 784, 831, 880};


Memoire24CXXX mem;
PWM mouvement;
uint8_t donnee = 0;
uint8_t adr = 0;
uint8_t compteur = 0;
uint8_t adrDbtBoucle = 0;
bool debut = false;

void attendre()
{
	mem.lecture(adr+1, &donnee);
	/*uint8_t tempsAttente = 25* donnee;
	_delay_ms(tempsAttente);*/
	
	for(uint8_t i = 0; i < donnee; ++i){
		_delay_ms(25);
	}
}

void allumerDel()
{
	mem.lecture(adr+1, &donnee);
	PORTB |= donnee;
}

void eteindreDel()
{
	mem.lecture(adr+1, &donnee);
	PORTB &= ~donnee;
}

void setPiezoFreq(uint16_t freq)
{
	TCCR0A = _BV(WGM01) | _BV(COM0A0) | _BV(COM0B0);
	if(freq > 450) {
		TCCR0B = _BV(CS01) | _BV(CS00);
		OCR0A = 62500 / freq - 1; //OCR0A + 1 = clk * 2 * prescaler / freq
	} else {
		TCCR0B = _BV(CS02);
		OCR0A = 15625 / freq - 1; //OCR0A + 1 = clk * 2 * prescaler / freq
	}
}

void jouerSonorite()
{
	mem.lecture(adr+1, &donnee);
	setPiezoFreq(notesMIDI[donnee-45]);
}

void arreterSonorite()
{
	TCCR0A = 0;
	TCCR0B = 0;
}

void arreterMoteurs()
{
	mouvement.changeDutyCycle(0);	
}

void debutBoucle()
{
	mem.lecture(adr+1, &compteur);
	adrDbtBoucle = adr;	
}

void finBoucle()
{
	if(compteur != 0)
	{
		compteur--;
		adr= adrDbtBoucle;
	}
}

void avancer()
{
	mem.lecture(adr+1, &donnee);
	mouvement.changeDutyCycle(donnee);
	PORTD |= _BV(PD6) | _BV(PD7);
}

void reculer()
{
	mem.lecture(adr+1, &donnee);
	mouvement.changeDutyCycle(donnee);
	PORTD &= ~(_BV(PD6) | _BV(PD7));
}

void tournerDroite()
{
	PORTD |= _BV(PD7);
	PORTD &= ~_BV(PD6);
	mouvement.changeDutyCycle(200);
	_delay_ms(500);
	mouvement.changeDutyCycle(0);
}

void tournerGauche()
{
	PORTD |= _BV(PD6);
	PORTD &= ~_BV(PD7);
	mouvement.changeDutyCycle(200);
	_delay_ms(500);
	mouvement.changeDutyCycle(0);
}


int main()
{
		DDRD = 0xFF;
		DDRB= 0xFF;
		
		while (donnee != 0b11111111)
		{
			mem.lecture(adr, &donnee);
			switch(donnee){
				case 0b1:	debut = true;
							break;
							
				case 0b10:	if(debut) attendre();
							break;
							
				case 0b1000100:	if(debut) allumerDel();
								break;
								
			
				case 0b1000101:	if(debut) eteindreDel();
								break;
								
				case 0b1001000:	if (debut) jouerSonorite();
								break;
								
			    case 0b1001:	if (debut)	arreterSonorite();
								break;
							
			    case 0b11000000: if (debut) debutBoucle();
								break;
							
			    case 0b11000001:	if(debut) finBoucle();
									break;
								
			    case 0b1100000:	if(debut) arreterMoteurs();
								break;
								
				case 0b1100001:	if(debut) arreterMoteurs();
								break;
								
				case 0b1100010:	if (debut)	avancer();	
								break;
								
				case 0b1100011:	if (debut)	reculer();
								break;	
								
				case 0b1100100:	if (debut)	tournerDroite();
								break;
								
				case 0b1100101:	if (debut)	tournerGauche();
								break;				
				
			}
			adr+=2;
		}
		return 0;
}

