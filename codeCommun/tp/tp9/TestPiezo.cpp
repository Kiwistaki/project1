#define F_CPU 8000000

#include "../tp8/lib/uart.h"
#include "../tp8/lib/can.h"
#include "../tp8/lib/pwm.h"
#include "../tp8/lib/memoire_24.h"

#include "util/delay.h"
#include "stdlib.h"


uint16_t notesMIDI[37] = {110, 117, 123, 131, 139, 147, 156, 165, 175, 185, 196, 208, 220, 
						  233, 247, 262, 277, 294, 311, 330, 350, 370, 392, 415, 440, 
						  466, 494, 523, 554, 587, 622, 659, 698, 740, 784, 831, 880};

//La sortie PWM est sur les pins B4 et B5
void setPiezoFreq(uint16_t freq)
{
	TCCR0A = _BV(WGM01) | _BV(COM0A0) | _BV(COM0B0);
	if(freq > 450) {
		TCCR0B = _BV(CS01) | _BV(CS00);
		OCR0A = 62500 / freq - 1; //OCR0A + 1 = clk * 2 * prescaler / freq
	} else {
		TCCR0B = _BV(CS02);
		OCR0A = 15625 / freq - 1; //OCR0A + 1 = clk * 2 * prescaler / freq
	}
}

void stopPiezo()
{
	TCCR0A = 0;
	TCCR0B = 0;
}
int main()
{	
	DDRB = 0xFF;
	
	uint16_t nbNotes = 381;
	uint16_t msParDelta = 1000;
	uint8_t note = 0;
	uint8_t delta = 0;
	uint8_t wait = 0;
	
	Memoire24CXXX mem;
	
	//mem.lecture(0, (uint8_t*) &nbNotes, 4);
	//mem.lecture(4, (uint8_t*) &msParDelta, 4);

	_delay_ms(3000);
	
	/*for(uint16_t i = 0; i < (uint16_t)(nbNotes); ++i) {
		//Environ 1.8ms pour lire trois fois en mémoire
		mem.lecture(i*3 + 6, &wait);
		mem.lecture(i*3 + 7, &note);
		mem.lecture(i*3 + 8, &delta);
		
		for(uint16_t j = 0; j < (uint16_t)(((uint32_t)wait*(uint32_t)msParDelta) >> 6); ++j) {
			_delay_ms(1);
		}
		setPiezoFreq(notesMIDI[note - 45]);
		for(uint16_t j = 0; j < ((delta*(uint16_t)msParDelta) >> 6); ++j) {
			_delay_ms(1);
		}
		stopPiezo();
	}*/
    for(;;)
    {
        for(uint8_t i = 0; i < 37; i++){
            setPiezoFreq(notesMIDI[i]);
            _delay_ms(50);
        }
    }
}
