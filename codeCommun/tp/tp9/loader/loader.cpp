/* Auteurs : Érix Morisette et Félix Prévost
 * 
 * Ce programme charge un fichier reçu par rs-232 sur la mémoire
 * externe de la carte mère
 */

#include "../../tp8/lib/memoire_24.h"
#include "../../tp8/lib/uart.h"

int main()
{
	Memoire24CXXX mem;
	UART com;
	
	uint16_t adr = 0;
	uint8_t donnee = 0;
	
	uint16_t taille = 0;
	
	taille = com.receptionUART();
	taille = taille << 8;
	taille = taille + com.receptionUART();
	
	for(uint16_t i = 0; i < taille-2; ++i) {
		donnee = com.receptionUART();
		mem.ecriture(adr, donnee);
		++adr;
	}
}
