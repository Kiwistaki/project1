#define F_CPU 8000000
#include <util/delay.h>
#include <avr/io.h> 
#include "memoire_24.h"
#include <string.h>

#define PHRASE "*E*C*O*L*E* *P*O*L*Y*T*E*C*H*N*I*Q*U*E*\0"
#define VERT 0x02
#define ROUGE 0x01
#define LONGUEUR 40
#define ADRESS 0x00

int main()
{
	DDRD = 0xFF;
	Memoire24CXXX message;
	uint8_t donnees[LONGUEUR+1]= PHRASE;
	uint8_t donneesLues[LONGUEUR+1];
		
	message.ecriture(ADRESS,donnees,LONGUEUR);
	_delay_ms(5);
	message.lecture(ADRESS, donneesLues, LONGUEUR);
	
	if(strcmp((char*)donnees,(char*)donneesLues)==0)
	{
		PORTD= VERT;
	}else{
		PORTD= ROUGE;
	}
	

	return 0;
	
}
