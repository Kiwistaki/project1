/*
 * |État présent | bouton | État suivant | sortie (Port A)
 * |_________________________________________________________
 * |init         | 0      | inter0       | 0xAA
 * |init         | 1      | inter0       | 0xAA-0x55
 * |inter0       | 0      | etat1        | 0x55
 * |etat1        | 1      | inter1       | 0xAA
 * |inter1       | 0      | etat2        | 0x00
 * |etat2        | 1      | inter2       | 0x55
 * |inter2       | 0      | init         | 
 * 
*/

#define F_CPU 8000000
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/io.h> 

enum Etat {init, inter0, etat1, inter1, etat2, inter2};
volatile uint8_t etat=0;
volatile Etat present= init;


ISR(INT0_vect){
    _delay_ms(30);
	    //a completer
    switch (present)
	{
	  case init: PORTA= 0xAA; 
		     present=inter0;
		     break;
	  
	  case etat1: PORTA= 0x55;
		      present=inter1;
		      break;

	  case etat2: PORTA=0x00;
		      present=inter2;
		      break;

	  case inter0:  while(PIND & 0x04)  //changer la condition du while
			{
			    PORTA = 0xAA;
			    _delay_ms(1);
			    PORTA = 0x55;
			    _delay_ms(1);
			}
			present=etat1;
			break;
	  
	  case inter1: while(PIND & 0x04)
			{
			  PORTA=0xAA;
			}
			present= etat2;
		        break;

	  case inter2: while(PIND & 0x04)
			{
			  PORTA=0x55;
			}
			present= init;
			break;
	
	  default: break;
	}
    EIFR |= (1 << INTF0);
}
void initialisation(){
    cli();
    DDRA = 0xff;
    DDRD= 0x00;
    EIMSK |= _BV(INT0);
    EICRA |= IINTF0;                        //a verifer
    sei();
}

int main()
{

  
  initialisation();
  for(;;)
  {	
  }
  return 0;
} 
