
#define F_CPU 8000000
#include <util/delay.h>
#include <avr/interrupt.h>
#include <avr/io.h> 



void ajustementPWM(uint8_t duty)
{
	OCR1A= duty;
    OCR1B= duty;
	TCCR1A |= _BV(COM1A0) | _BV(COM1B0) | _BV(COM1A1) | _BV(COM1B1) | _BV(WGM10);

    TCCR1B |= _BV(CS11);
}

void initialisation()
{
    cli();
    DDRD= 0xff;
    EIMSK |= _BV(INT0);
    EICRA |= _BV(ISC00);                       
    sei();
}

int main()
{
  initialisation();
  uint8_t dutyCycle = 0;
  
  while(1)
  {
    ajustementPWM(dutyCycle);
    
    dutyCycle += 64;
    if (dutyCycle == 256)
    {
		 dutyCycle = 255;
    }else if(dutyCycle == 319)
    {
		 dutyCycle = 0;
	}
	
	_delay_ms(3000);
  }
  
  return 0;
}
