/*
 * |État présent | bouton | État suivant | sortie (Port A)
 * |_________________________________________________________
 * |init         | 1      | inter0       | 0x00
 * |inter0       | 0      | etat1        | 0x00
 * |etat1        | 1      | inter1       | 0x00
 * |inter1       | 0      | etat2        | 0x00
 * |etat2        | 1      | inter2       | 0x00
 * |inter2       | 0      | etat3        | 0x00
 * |etat3        | 1      | inter3       | 0x00
 * |inter3       | 0      | etat4        | 0x00
 * |etat4        | 1      | init         | 0xAA
 * 
*/
#define F_CPU 8000000
#include <util/delay.h>

#include <avr/io.h> 

enum Etat {init, inter0, etat1, inter1, etat2, inter2, etat3, inter3, etat4};


int main()
{
  DDRA = 0xff;
  DDRD= 0x00;
  Etat present= init;
  
  for(;;)
  {
    
    if (PIND & 0x04)
    {
      _delay_ms(50);
      if (PIND & 0x04)
      {
	switch (present)
	{
	  case init: present=inter0;
		     break;
	  
	  case etat1: present= inter1;
		      break;

	  case etat2: present= inter2;
		      break;

	  case etat3: present= inter3;
		      break;

	  case etat4: present=init;
		      PORTA= 0xAA;
		      _delay_ms(1000);
		      PORTA= 0x00;
		      break;
	  default: break;
	}
      }
      else{
	switch (present)
	{
	  case inter0: present=etat1;
			break;
	  
	  case inter1: present= etat2;
		       break;

	  case inter2: present= etat3;
			break;

	  case inter3: present= etat4;
			break;
	
	  default: break;
	}
      }
    }

  }
  return 0;
}