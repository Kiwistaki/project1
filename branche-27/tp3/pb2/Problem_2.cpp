/*
 * |État présent | bouton | État suivant | sortie (Port A)
 * |_________________________________________________________
 * |init         | 0      | inter0       | 0xAA
 * |init         | 1      | inter0       | 0xAA-0x55
 * |inter0       | 0      | etat1        | 0x55
 * |etat1        | 1      | inter1       | 0xAA
 * |inter1       | 0      | etat2        | 0x00
 * |etat2        | 1      | inter2       | 0x55
 * |inter2       | 0      | init         | 
 * 
*/

#define F_CPU 8000000
#include <util/delay.h>

#include <avr/io.h> 

enum Etat {init, inter0, etat1, inter1, etat2, inter2};


int main()
{
  DDRA = 0xff;
  DDRD= 0x00;
  Etat present= init;
  
  for(;;)
  {
    
	switch (present)
	{
	  case init: PORTA= 0xAA; 
		      if (PIND & 0x04)
		      {
			_delay_ms(50);
			if (PIND & 0x04)
			  {
			    present=inter0;
			  }
		      }
		     break;
	  
	  case etat1: PORTA= 0x55;
		      if (PIND & 0x04)
			{
			  _delay_ms(50);
			  if (PIND & 0x04)
			  {
			    present=inter1;
			  }
			}
		      break;

	  case etat2: PORTA=0x00;
		      if (PIND & 0x04)
		      {
			 _delay_ms(50);
			if (PIND & 0x04)
			{
			  present=inter2;
			}
		      }
		      break;

	  case inter0:  while(PIND & 0x04)
			{
			    PORTA = 0xAA;
			    _delay_ms(1);
			    PORTA = 0x55;
			    _delay_ms(1);
			}
			present=etat1;
			break;
	  
	  case inter1: while(PIND & 0x04)
			{
			  PORTA=0xAA;
			}
			present= etat2;
		        break;

	  case inter2: while(PIND & 0x04)
			{
			  PORTA=0x55;
			}
			present= init;
			break;
	
	  default: break;
	}
      }

  return 0;
} 
