
#define F_CPU 8000000
#include <avr/io.h> 
#include <util/delay.h>

int main()
{
  DDRD = 0xff; // PORT D est en mode sortie
  int tempsAllume=1500;
  int tempsEteint=0;
  
  while(1)  // boucle sans fin
  {
	
    for(int i = 0; i < tempsAllume ;i++)
    {
      PORTD=0xAA;  
    }
    
    for( int j = 0; j< tempsEteint; j++)
    {
      PORTD=0x00; 
    }
    
      tempsAllume--;
      tempsEteint++;
      
      if(tempsAllume == 0)
      {
       tempsAllume=1500;
       tempsEteint=0;
       _delay_ms(1000);
      }
    
  }
  return 0;
}

 
