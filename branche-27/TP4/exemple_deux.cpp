 
#define F_CPU 8000000
#include <avr/io.h> 
#include <util/delay.h>

int main()
{
  DDRD = 0xff; // PORT D est en mode sortie
  
  while(1)  // boucle sans fin
  {
    //pour 60Hz
	for (int i=0; i < 118; i++)
	{
	  _delay_ms(17);
	  PORTD=0x00;
	  _delay_ms(0);
	  PORTD=0x10;
	}
	
	for (int i=0; i < 118; i++)
	{
	  _delay_ms(12.75);
	  PORTD=0x00;
	  _delay_ms(4.25);
	  PORTD=0x10;
	}
	
	for (int i=0; i < 118; i++)
	{
	  _delay_ms(8.5);
	  PORTD=0x00;
	  _delay_ms(8.5);
	  PORTD=0x10;
	}
	
	for (int i=0; i < 118; i++)
	{
	  _delay_ms(4.25);
	  PORTD=0x00;
	  _delay_ms(12.75);
	  PORTD=0x10;
	}
	
	for (int i=0; i < 118; i++)
	{
	  _delay_ms(0);
	  PORTD=0x00;
	  _delay_ms(17);
	  PORTD=0x10;
	}
	
    //Pour 400Hz
    for (int i=0; i < 800; i++)
	{
	  _delay_ms(2.5);
	  PORTD=0x00;
	  _delay_ms(0);
	  PORTD=0x10;
	}
	
	for (int i=0; i < 800; i++)
	{
	  _delay_ms(1.75);
	  PORTD=0x00;
	  _delay_ms(0.75);
	  PORTD=0x10;
	}
	
	for (int i=0; i < 800; i++)
	{
	  _delay_ms(1.25);
	  PORTD=0x00;
	  _delay_ms(1.25);
	  PORTD=0x10;
	}
	
	for (int i=0; i < 800; i++)
	{
	  _delay_ms(0.75);
	  PORTD=0x00;
	  _delay_ms(1.75);
	  PORTD=0x10;
	}
	
	for (int i=0; i < 800; i++)
	{
	  _delay_ms(0);
	  PORTD=0x00;
	  _delay_ms(2.5);
	  PORTD=0x10;
	}
	
	
  }
  return 0;
}
