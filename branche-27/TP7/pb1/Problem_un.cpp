/////////////////////////////////////////////////////////////
//	Titre:  Problème un du TP7
//	Auteur: Yasmine Bekkouche et Alexandre Paquet
//	Date: 18 février 2013
//	Description: Le programme permet l'affichage de couleur
//				 selon l'état de l'interrupteur placé sur le 
//				 breadbord et selon un compteur
/////////////////////////////////////////////////////////////
//Remarque: Le fil de sortie du Breadboard est branché dans 
//			le port D(D0-D2). Le fil de sortie pour la DEL
//			est connecté de la façon suivante: C0 avec la 
//			borne positive de la DEL et C1 avec la borne 
//			négative de la DEL.
/////////////////////////////////////////////////////////////

#define F_CPU 8000000  //règle la fréquence d'horloge à 8MHz

#include <util/delay.h>
#include <avr/io.h> 

int main()
{
	DDRD = 0x00;	// Initialisation du PORT D en entrée
	DDRC = 0xFF;	// Initialisation du PORT C en sortie
	
	uint8_t compteur = 0;	//Initialisation d'un compteur
	bool estActif = false;	//Initialisation d'un booléen permettant de vérifier si
							// l'interrupteur a été appuyé une fois
	
	while(1) //boucle à condition infinie
	{
		while( !(PIND & _BV(PD2)) && compteur < 120) //condition selon si l'interrupteur
													 // est appuyé ET que le compteur
													 // est plus petit que 120
		{
			estActif = true;
			_delay_ms(100); //délai permettant l'incrémentation au nombre de 10 par seconde
			compteur++;  //incrémentation du compteur
		}
		
		if(((PIND & _BV(PD2)) || compteur == 120) && estActif) //condition de vérification
															   // si l'interrupteur est relaché
															   // OU si le compteur est égal
															   //a 120 ET que le bouton soit actif
		{
			// Affichage de la DEL de couleur vert pendant 1/2 seconde
			PORTC |= _BV(PC1);
			_delay_ms(500);
			PORTC &= ~_BV(PC1);
			_delay_ms(2000);
			int newCounter = (compteur/2);
			for(int i = 0; i < newCounter; i++)	//boucle au nombre du compteur divisé par 2
			{
				//affichage de la DEL de couleur rouge au rythme de 2 par seconde
				PORTC |= _BV(PC0);
				_delay_ms(250);
				PORTC &= ~_BV(PC0);
				_delay_ms(250);
				PORTC |= _BV(PC0);
				_delay_ms(250);
				PORTC &= ~_BV(PC0);
				_delay_ms(250);
			}
			//// Affichage de la DEL de couleur vert pendant 1 seconde
			PORTC |= _BV(PC1);
			_delay_ms(1000);
			PORTC &= ~_BV(PC1);
			
			if(compteur==120)
			{
				compteur = 0;	//remise à zéro du compteur
			}
			estActif = false;  //remise a false du booléen afin d'empêcher le retour dans la condition if
		}

	}	

	return 0;	
}
