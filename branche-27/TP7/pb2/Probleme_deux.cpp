/////////////////////////////////////////////////////////////
//	Titre:  Problème deux du TP7
//	Auteur: Yasmine Bekkouche et Alexandre Paquet
//	Date: 18 février 2013
//	Description: Le programme permet l'affichage de couleur
//				 selon l'intensité lumineuse captée par le
//				 sensor photosensible
/////////////////////////////////////////////////////////////

#define F_CPU 8000000		//Réglage de la vitesse d'horloge à 8MHz
#include <avr/io.h>
#include <avr/delay.h>
#include "can.h"

int main()
{
	DDRA = 0xFF;  //PORTA en sortie
	DDRD = 0x00;	//PORTD en entrée
	can convertAnaNum;  // variable de type can qui va permettre la lecture du sensor photosensible

	for(;;)
	{
		uint16_t valeurDuConvertisseur= convertAnaNum.lecture(0);  //Variable servant à la lecture du sensor photosensible qui
		                                                           //retourne une valeur de 10 bits sur un entier non-signé de 16 bits

		if( valeurDuConvertisseur <= )    //Comparaison de la valeur de retour pour évaluer si l'intensité lumineuse est basse
		{
			PORTA = 0x02;   //Affichage de la DEL de couleur verte

		}else if( valeurDuConvertisseur >=){  //Comparaison de la valeur de retour pour évaluer si l'intensité lumineuse est élevée

			PORTA = 0x01;  //Affichage de la DEL de couleur rouge

		}else{

			//Affichage de couleur ambrée pour la DEL
			PORTA = 0x02;
			_delay_ms(15);
			PORTA = 0x01;
			_delay_ms(5);

		}
	}

	return 0;
}